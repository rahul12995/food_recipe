import { useState, useEffect, useRef } from 'react'
import axios from 'axios';

const List = () => {
    const [listData, setListData] = useState([]);
    let inputRef = useRef()
    useEffect(() => {
        axios.get(`https://api.edamam.com/search?app_id=900da95e&app_key=40698503668e0bb3897581f4766d77f9&q=mango`)
            .then((data) => {
                if (data && data.data && data.data.hits) {
                    setListData(data.data.hits)
                }
            })
    }, [])

    const searchRecipe = () => {
        const value = inputRef.current.value
        if (value) {
            axios.get(`https://api.edamam.com/search?app_id=900da95e&app_key=40698503668e0bb3897581f4766d77f9&q=${value}`)
                .then((data) => {
                    if (data && data.data && data.data.hits) {
                        setListData(data.data.hits)
                    }
                })
        }
    }

    const onKeyUp = (e) => {
        const { keyCode } = e;
        if (keyCode === 13) {
            searchRecipe()
        }
    }

    return (<div className="main-wrapper">
        <h2>Food Recipe App</h2>
        <div className="search-bar">
            <input onKeyUp={onKeyUp} ref={inputRef} />
            <button onClick={searchRecipe} >Search</button>
        </div>
        <div className="list-data">
            {
                listData.map((item) => <div className="inner-div">
                    <a href={item.recipe.shareAs} target="_blank"><img src={item.recipe.image} /></a>
                    <div>
                        <h5>{item.recipe.label}</h5>
                    </div>

                </div>)
            }
            {
                !listData || !listData.length && "No Data Found"
            }
        </div>

    </div>)
}


export default List;